// src/components/UpdateUser.js
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom'; // Importe o useParams

function UpdateUser() {
  const { id } = useParams(); // Use o hook useParams para obter o ID da rota
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');

  useEffect(() => {
    axios.get(`http://localhost:8080/users/${id}`)
      .then(response => {
        const user = response.data;
        setName(user.name);
        setEmail(user.email);
      })
      .catch(error => {
        console.error('Error fetching user data:', error);
      });
  }, [id]); // Certifique-se de incluir id na lista de dependências do useEffect

  const handleSubmit = (e) => {
    e.preventDefault();
    axios.put(`http://localhost:8080/users/${id}`, { name, email })
      .then(() => {
        alert('User updated successfully');
      })
      .catch(error => {
        console.error('Error updating user:', error);
      });
  };

  return (
    <div>
      <h2>Update User</h2>
      <form onSubmit={handleSubmit}>
        <label>Name:</label>
        <input type="text" value={name} onChange={(e) => setName(e.target.value)} required />
        <label>Email:</label>
        <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} required />
        <button type="submit">Update</button>
      </form>
    </div>
  );
}

export default UpdateUser;

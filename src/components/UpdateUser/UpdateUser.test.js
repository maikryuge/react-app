// No início do seu arquivo de teste, antes dos testes:
const originalAlert = window.alert;
const originalConsoleError = console.error;

// No final do seu arquivo de teste, após os testes:
afterAll(() => {
  window.alert = originalAlert;
  console.error = originalConsoleError;
});

// Dentro do teste que verifica o alerta:
test('updates user on form submission', async () => {
  const mockId = '123'; // Sample ID
  useParams.mockReturnValue({ id: mockId });

  const mockedAxios = axios.put.mockResolvedValueOnce();
  render(<UpdateUser />);
  
  const nameInput = screen.getByLabelText(/name/i);
  const emailInput = screen.getByLabelText(/email/i);
  const updateButton = screen.getByRole('button', { name: /update/i });

  fireEvent.change(nameInput, { target: { value: 'Updated Test User' } });
  fireEvent.change(emailInput, { target: { value: 'updated@example.com' } });
  fireEvent.click(updateButton);

  await waitFor(() => {
    expect(mockedAxios).toHaveBeenCalledWith(`http://localhost:8080/users/${mockId}`, {
      name: 'Updated Test User',
      email: 'updated@example.com',
    });
    expect(window.alert).toHaveBeenCalledWith('User updated successfully');
  });

  // Substitua o window.alert por uma função mock para rastrear sua chamada
  window.alert = jest.fn();
});

// Dentro do teste que verifica o console.error:
test('handles errors during user update', async () => {
  const mockId = '123'; // Sample ID
  useParams.mockReturnValue({ id: mockId });

  const errorMessage = 'Error updating user';
  axios.put.mockRejectedValueOnce(new Error(errorMessage));
  render(<UpdateUser />);
  
  const updateButton = screen.getByRole('button', { name: /update/i });

  fireEvent.click(updateButton);

  await waitFor(() => {
    expect(console.error).toHaveBeenCalledWith(`Error updating user: ${errorMessage}`);
  });

  // Substitua o console.error por uma função mock para rastrear sua chamada
  console.error = jest.fn();
});
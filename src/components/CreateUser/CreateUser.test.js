// No início do seu arquivo de teste, antes dos testes:
const originalAlert = window.alert;
const originalConsoleError = console.error;

// No final do seu arquivo de teste, após os testes:
afterAll(() => {
  window.alert = originalAlert;
  console.error = originalConsoleError;
});

// Dentro do teste que verifica o alerta:
test('creates user on form submission', async () => {
  const mockedAxios = axios.post.mockResolvedValueOnce();
  render(<CreateUser />);
  
  const nameInput = screen.getByLabelText(/name/i);
  const emailInput = screen.getByLabelText(/email/i);
  const createButton = screen.getByRole('button', { name: /create/i });

  fireEvent.change(nameInput, { target: { value: 'Test User' } });
  fireEvent.change(emailInput, { target: { value: 'test@example.com' } });
  fireEvent.click(createButton);

  await waitFor(() => {
    expect(mockedAxios).toHaveBeenCalledWith('http://localhost:8080/users', {
      name: 'Test User',
      email: 'test@example.com',
    });
    expect(nameInput).toHaveValue('');
    expect(emailInput).toHaveValue('');
    expect(window.alert).toHaveBeenCalledWith('User created successfully');
  });

  // Substitua o window.alert por uma função mock para rastrear sua chamada
  window.alert = jest.fn();
});

// Dentro do teste que verifica o console.error:
test('handles errors during user creation', async () => {
  const errorMessage = 'Error creating user';
  axios.post.mockRejectedValueOnce(new Error(errorMessage));
  render(<CreateUser />);
  
  const createButton = screen.getByRole('button', { name: /create/i });

  fireEvent.click(createButton);

  await waitFor(() => {
    expect(console.error).toHaveBeenCalledWith(`Error creating user: ${errorMessage}`);
  });

  // Substitua o console.error por uma função mock para rastrear sua chamada
  console.error = jest.fn();
});
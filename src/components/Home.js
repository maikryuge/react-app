import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom'; // Importe o componente Link

function Home() {
  const [userData, setUserData] = useState(null);

  useEffect(() => {
    axios.get('http://localhost:8080/users')
      .then(response => {
        setUserData(response.data);
      })
      .catch(error => {
        console.error('Error fetching user data:', error);
      });
  }, []);

  return (
    <div>
      <h1>User Data</h1>
      {/* Adicione um link para a rota de criação de usuário */}
      <Link to="/users/create">Create User</Link>
      
      {userData ? (
        <ul>
          {userData.map(user => (
            <li key={user.id}>
              {user.name} -{' '}
              {/* Adicione um link para a rota de atualização de usuário para cada usuário */}
              <Link to={`/users/${user.id}/edit`}>Update</Link>
            </li>
          ))}
        </ul>
      ) : (
        <p>Loading...</p>
      )}
    </div>
  );
}

export default Home;

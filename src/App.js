// src/App.js
import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'; // Alterado para Routes
import Home from './components/Home';
import CreateUser from './components/CreateUser/CreateUser';
import UpdateUser from './components/UpdateUser/UpdateUser';
import './App.css';

function App() {
  return (
    <Router>
      <div className="App">
        <Routes> {/* Alterado para Routes */}
          <Route exact path="/" element={<Home />} /> {/* Alterado para element */}
          <Route exact path="/users/create" element={<CreateUser />} /> {/* Alterado para element */}
          <Route exact path="/users/:id/edit" element={<UpdateUser />} /> {/* Alterado para element */}
        </Routes> {/* Alterado para Routes */}
      </div>
    </Router>
  );
}

export default App;
